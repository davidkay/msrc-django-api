from msrc.apps.core.renderers import msrcJSONRenderer


class ArticleJSONRenderer(msrcJSONRenderer):
    object_label = 'article'
    pagination_object_label = 'entities'
    pagination_count_label = 'articlesCount'


class CommentJSONRenderer(msrcJSONRenderer):
    object_label = 'comment'
    pagination_object_label = 'comments'
    pagination_count_label = 'commentsCount'
