from django.apps import AppConfig


class ArticlesAppConfig(AppConfig):
    name = 'msrc.apps.entities'
    label = 'entities'
    verbose_name = 'Articles'

    def ready(self):
        import msrc.apps.entities.signals

default_app_config = 'msrc.apps.entities.ArticlesAppConfig'
