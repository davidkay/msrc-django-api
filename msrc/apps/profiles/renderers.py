from msrc.apps.core.renderers import msrcJSONRenderer


class ProfileJSONRenderer(msrcJSONRenderer):
    object_label = 'profile'
    pagination_object_label = 'profiles'
    pagination_count_label = 'profilesCount'
